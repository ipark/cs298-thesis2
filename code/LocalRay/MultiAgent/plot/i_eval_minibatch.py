import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

#df["rolling"] = df["number_users"].rolling(1).mean()

data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'sa_ppo.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-i3Walkers [400,300] Ref.***', linewidth=3, color='cyan', alpha=0.6)

data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_5ec2f_00000_0_2020-10-28_18-45-06'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker', linewidth=2, alpha=0.6, color='orange')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_i2walkers_76bcd_00000_0_2020-10-29_23-45-14'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i2Walkers', linewidth=2, color='red', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i2walkers_40d96_00000_0_2020-10-30_08-40-35'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i2Walkers(2)', linewidth=2, color='red', alpha=0.3)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_f7057_00000_0_2020-11-01_15-50-04'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i3Walkers', linewidth=2, color='blue', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_259cb_00000_0_2020-10-30_18-19-39'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i4Walkers', linewidth=2, color='green', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_952fc_00000_0_2020-11-03_10-37-10'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i4Walkers[long]', linewidth=2, color='green', alpha=0.3)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Environment: Multi-Walkers | Network Model: Hidden[400,300] w/ Relu \n Policy: PPO; Independent Learning; ReplayBuffer:50K; MinibatchSize:5K')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000, 110000, 120000, 130000, 140000, 150000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k', '110k', '120k', '130k', '140k', '150k'])

plt.xlim(0, 200000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-150, 50)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()
