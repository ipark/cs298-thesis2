import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os


data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'PPO1.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers [400,300] Ref.***', linewidth=3, color='cyan', alpha=0.6)


data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_8f6c3_00000_0_2020-10-24_22-54-35'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers (miniBatchSize:500)', linewidth=2, color='pink')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_15204_00000_0_2020-10-27_16-21-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers (miniBatchSize:5,000)', linewidth=2, color='red')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

#data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_e913d_00000_0_2020-10-28_12-15-15'
#df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
#x = df['episodes_total']
#y = df['episode_reward_mean']
#plt.plot(x, y, label='PPO-2Walkers (miniBatchSize:5,000) tanh', linewidth=2, color='black')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_798b1_00000_0_2020-10-28_09-13-11'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers (miniBatchSize:50,000)', linewidth=2, color='black')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Parameter Sharing : PPO Hidden[400,300] Relu Varying MiniBatchSize')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k'])

plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-200, 50)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()

