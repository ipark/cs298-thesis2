import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os


data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_44dd4_00000_0_2020-10-14_16-02-01'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-2Walkers [600,300,150]', linewidth=1, color='red')

data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_a476a_00000_0_2020-10-23_16-44-07'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-2Walkers Box(32,);Relu;', linewidth=1)

data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'PPO1.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-3Walkers [400,300] Ref.***', linewidth=3, color='cyan')

data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_4d14e_00000_0_2020-09-30_07-21-59'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-2Walkers [400,300]', linewidth=1, color='gray')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_bf0cc_00000_0_2020-09-29_22-28-18'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-3Walkers [400,300]', linewidth=1, color='blue')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_4walkers_df112_00000_0_2020-09-30_14-49-52'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-4Walkers [400,300]', linewidth=1, color='black')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('ParamSharing')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k'])
plt.xlim(0, 60000)

plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-200, 50)
plt.legend(loc='lower center')
plt.margins(x=0)

plt.show()

