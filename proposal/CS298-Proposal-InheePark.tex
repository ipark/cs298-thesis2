\documentclass[conference,11pt]{CS298}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnot. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{pdfpages}
\usepackage{listings} % needed for the inclusion of source code
\usepackage{mips}
\usepackage[capitalise]{cleveref}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}
\renewcommand\figurename{\textsc{Figure}}
\crefname{figure}{\textsc{Figure}}{\textsc{Figures}} 
\crefname{table}{\textsc{Table}}{\textsc{Tables}} 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\Large\textbf{CS 298 Proposal: Optimal Job Shop Scheduling with Deep Reinforcement Learning}}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park \texttt{(inhee.park@sjsu.edu)}}
	\IEEEauthorblockA{\textit{Department of Computer Science},
        \textit{San Jose State University}, San Jose, CA, USA}}
\maketitle
\thispagestyle{plain}
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. A brief abstract of your project.
%2. A list of project deliverables.
%3. A brief description of innovative and challenging aspects of your project.
%4. A schedule.
%5. A list of literature references.
%\begin{abstract}
%\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{IEEEkeywords}
%\end{IEEEkeywords}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
\section*{\large\textbf{1. A brief abstract of your project.}\raggedright}
\noindent
\subsection*{Summary of CS297 Result. }
\noindent
Fundamentals of Reinforcement Learning (RL) has been reviewed: 
essential terminologies, underlying mathematical equations and comparative summary of
vanilla RL methods in terms of features and pseudo-codes.  Plus, some quintessential
grid-based systems were solved by various RL methods demonstrating importance of devising
more advanced RL algorithms to tackle more challenging systems.

\noindent
\subsection*{Summary of CS298 Proposal. }
\noindent
I select the \textbf{Reinforcement Learning} as a main technical method, because RL is a method of learning how to learn.
While Deep Learning or Machine Learning learns about the model by one-shot feedback (either success or fail with respect to the true labels), RL uses an ``agent'' and let it learn about environment by sequential feedback as a penalty or credit per action.

\noindent
Equipped with the fundamentals of RL methods 
(Refs: \cite{DRLbook2020},\cite{JSSP-DRL},\cite{JSSP-2000}),
I will use the \emph{state-of-art} Deep RL method --- Actor-Critic DRL method 
(Refs: \cite{JSSP-AC,JSSP-DRL}) ---
to solve one of the \emph{classic} NP-hard problems --- Job Shop Scheduling Problem 
(Refs: \cite{JSSP-1995a,JSSP-1995b}).

\noindent
\subsection*{Job Shop Scheduling Problem (JSSP). }
\noindent
The JSSP is a sequential decision making problem, being one of the classical NP-hard problems in computer science.
It is an optimization problem in \emph{operational research}.  For example, to complete the mission in NASA, they should find an optimal scheduling to assign resources/personnel in charge of the time-sensitive tasks within allotted time. Given the multiple tasks with their temporal constraints (sequence of tasks matter) and available resource
constrains, the aim of JSSP is to find the optimal solution to fulfil all the given constraints and to complete
them all as short time as possible.
Due to the sheer number of tasks and constrains, the JSSP is a NP-hard problem, traditionally approached
by heuristic approximation methods.  Moreover, such heuristic method is rather planning (assuming 
static condition) not learning (adaptive to the change of conditions).
In real world scenarios, the given environment of the job tasks can be changed in real time,
such as unexpected pandemic like COVID-19, unexpected power outage due to wild-fire etc.
As a result, some resources might not be available. 
Therefore, we should find the adaptive learning method to deal with the stochastic, dynamic environment.

\noindent
\subsection*{Deep Reinforcement Learning (DRL). }
\noindent
Curse of dimensionality problem comes either from discrete but high-dimensional state space; or relatively low-dimensional but continuous state space. It is infeasible to exhaustively enumerate such state.\\

\noindent
As to increase the dimensionality of action space in JSSP (job dispatching rules),
DRL should be used instead of RL. 
\textbf{Deep} of \textbf{D}RL refers to non-linear function approximation.
Instead of explicitly update the value-function at each state of state-action pair visited in RL, 
the DRL updates value-function as a whole by non-linear function approximator using neural network (aka DRL)
to obtain the parametrized policy.
For the DNN part of DRL, input layer consists of states and the output layer gives probability of actions.

\noindent
\subsection*{Actor-Critic DRL Method. }
\noindent
In the CS297 report, all the RL methods aim at estimate of the value-function.
Since value-function is a summary of expected return, which is an objective function to be maximized.
Given the fact that the final goal is to find the optimal \textbf{policy} from that value-function, i.e.
indirect approach to the policy.\\

\noindent
The \textbf{policy-gradient} method is a direct way of finding the policy.
\textbf{Actor-Critic} method is one of DRL algorithms combining policy-gradient method and value-function method.
Actor is a policy used to select actions by policy gradient.
Critic is a value function to estimate about goodness/badness of the actions made by the actor.\\

\noindent
Owing to excellent performance of the Actor-Critic method, recent publication in DRL field, it is frequently used to solve the various sequential decision problems.
The most recent paper \cite{JSSP-AC} also used the Actor-Critic method to solve the JSSP.
Specially  that paper used DDPG (Deep Deterministic Policy Gradient).
I will improve this part by using TD3 (Twin-Delayed DDPG) which incorporated
improvements over DDPG by adding stochasticity in selecting actions and
allowing some delay in update the value-function permitting more accurate estimate with reduced error.
%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
\section*{\large\textbf{2. A list of project deliverables.}\raggedright}
\begin{itemize}
    \item  CS298 Proposal 
    \item  Code and Dataset \\(Repo: \texttt{https://gitlab.com/ipark/cs298-thesis2})
    \item  CS298 Report 
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
\section*{\large\textbf{3. A brief description of innovative and challenging aspects of your project.}\raggedright}
\subsection*{Innovative Aspects. } 
\noindent
This project focuses more on the algorithmic efficiency rather than the application outcome per se.  With this purpose, I will try to articulate which aspects of the state-of-art Actor-Critic method is a driving force to turn the classic NP-hard JSSP into solvable optimal problem.  To achieve this goal, I will compare not only the underlying algorithm per se, but also compare the side-by-side pseudo-code comparison for clear view. Such approach is crucial to distinguish what have been done in the published work (Ref: \cite{JSSP-AC}) and what I can improve further.  Specifically with the DDPG method as a benchmarking basis, I will quantify the incremental effect by including a stochasticity in selecting action space, and by modifying the update frequency for the estimate of value-function.  After testing with each modification, then integrating those modifications, then I will be able to quantify the efficiency improvement achieved by the TD3 method (as an advanced version of DDPG). 
 
\subsection*{Challenging Aspects. }
\noindent
Computing resource might be a cause of bottleneck to achieve a final goal.
Prior to any computation, I should firstly analyse the size of available dataset. Then I should try to estimate the approximate computing time ahead of actual execution using the smaller dataset. Only after some feasibility check is deemed, I should use manageable size of dataset that are enough to observe the expected result, followed by fully set up the entire run for the whole dataset. \\

\noindent
In addition, it is unclear at the moment that the quality
of the proposed algorithms will be consistent when the size of
the dataset grows large.
%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
\section*{\large\textbf{4. A schedule.}\raggedright}
Detailed schedule is tabulated in Table~\ref{t:schedule}.

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
\begin{thebibliography}{00}
\bibitem{JSSP-AC}
C. Liu, C. Chang and C. Tseng, 
Actor-Critic Deep Reinforcement Learning for Solving Job Shop Scheduling Problems 
in IEEE Access, vol. 8, pp. 71752-71762, 2020.\\
\texttt{This is the most recent paper using DRL to solve JSSP}

\bibitem{JSSP-1995a}
Wei Zhang and Thomas G. Dietterich. 
A Reinforcement Learning Approach to Job-Shop Scheduling. 
In Proceedings of the 14th international joint conference on Artificial intelligence - Volume 2 (IJCAI'95).  Morgan Kaufmann Publishers Inc., San Francisco, CA, USA, 1114–1120, 1995.\\
\texttt{This is the first attempt applying RL method to JSS problem}

\bibitem{JSSP-1995b}
Wei Zhang and Thomas G. Dietterich. 
High-Performance Job-Shop Scheduling With A Time-Delay TD-lambda Network.
MIT Press, Advances in Neural Information Processing Systems 8, 1024--1030, 1995.\\
\texttt{This is also early attempt of using the improved RL method to JSS problem}

\bibitem{JSSP-2000}
M.Emin Aydin, Ercan \"Oztemel,
Dynamic job-shop scheduling using reinforcement learning agents,
Robotics and Autonomous Systems,
Volume 33, Issues 2–3, pp. 169-178, 2002.\\
\texttt{Job dispatching rules are used to serve action space in RL}

\bibitem{JSSP-DRL}
B. Cunha, A. Madureira, B. Fonseca and D. Coelho, Duarte. 
Deep Reinforcement Learning as a Job Shop Scheduling Solver: A Literature Review. 
In book: Intelligent Decision Support Systems --A Journey to Smarter Healthcare, pp.350-359, 2020.\\
\texttt{This is the most recent review paper on JSSP and DRL}


\bibitem{DRLbook2020}
Grokking Deep Reinforcement Learning;
Miguel Morales; Manning Early Access Program (MEAP); ISBN 9781617295454.
Publication in October 2020.\\
\texttt{This is a recent book on RL and DRL with accompanied python notebooks 
        https://github.com/mimoralea/gdrl/tree/master/notebooks}

\bibitem{RLbook}
Reinforcement Learning: An Introduction;
R. Sutton, and A. Barto; The MIT Press, Second edition, 2018.\\
\texttt{This is the classic RL book.}


\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{onecolumn}

\begin{table}[th!]
\centering
    \caption{Time Schedule for the Thesis Project}
\begin{tabular}{|c|c|l|l|}
\hline
Week & Date & Task & Deadline \\ 
\hline 
\hline 
1    & 8/19 - 8/22 & Actor-Critic RL Method Review (Ch11-12 in Ref: \cite{DRLbook2020}) &\\ 
\hline 
2    & 8/23 - 8/29 & 
\vtop{\hbox{\strut Actor-Critic JSSP Paper Review (Ref: \cite{JSSP-AC})}
\hbox{\strut Data Collection from Ref: \cite{JSSP-AC,JSSP-2000,JSSP-DRL}}}
&\\
\hline 
    3    & 8/30 - 9/05 & Start to Reproduce Actor-Critic JSSP Paper & 9/1: Graduation Application Deadline\\
\hline 
4    & 9/06 - 9/12 & Improvement 1: Add double learning to DDPG (Ref: \cite{DRLbook2020,JSSP-AC})  &\\
\hline 
5    & 9/13 - 9/19 & Improvement 2: Smoothing the targets used for policy updates  (Ref: \cite{DRLbook2020,JSSP-AC}) &\\
\hline 
6    & 9/20 - 9/26 & Improvement 3: Delaying updates to DDPG (Ref: \cite{JSSP-AC,DRLbook2020}) &\\
\hline 
7    & 9/27 - 10/03 & Integrated Improvement:  Replace DDPG with TD3 (Ref: \cite{JSSP-AC,DRLbook2020})&\\
\hline 
8    & 10/04 - 10/10 & Start to write Experiment section &\\
\hline 
9    & 10/11 - 10/17 & Start to write Result section &\\
\hline 
10   & 10/18 - 10/24 & Algorithm and Pseudo-code Comparison between DDPG vs. TD3 &\\
\hline 
11   & 10/25 - 10/31 & Start to write Discussion section &\\
\hline 
12   & 11/01 - 11/07 & Finalize Tables and Figures  &\\
\hline 
13   & 11/08 - 11/14 & Wrap up the thesis write-up & 11/09: Thesis Review  Deadline\\
\hline 
14   & 11/15 - 11/21 & Start to make defence presentation &\\
\hline 
15   & 11/22 - 11/28 & Continue to improve defence presentation &\\
\hline 
16   & 11/29 - 12/05 & Schedule the defence date and practice &\\
\hline 
17   & 12/06 - 12/12 & Defence & \\
\hline 
\hline 
-    & 1/11/2021   & Final edit the thesis document & Thesis Publication Deadline\\
\hline 
\end{tabular} 
\label{t:schedule}
\end{table}
%
%
\end{onecolumn}

%\subsection*{\textsc{\bfseries{ 
%}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[th!]
%	\centering
%	\includegraphics[width=\columnwidth]{./Figs/}
%	\caption{}
%	\label{f:}
%\end{figure} 



%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
