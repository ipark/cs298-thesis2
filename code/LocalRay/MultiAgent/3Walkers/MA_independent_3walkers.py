import sys
from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.pettingzoo_env import PettingZooEnv
from pettingzoo.sisl import multiwalker_v3

# Based on code from github.com/parametersharingmadrl/parametersharingmadrl

if __name__ == "__main__":
    def env_creator(args):
        ## ipark
        return PettingZooEnv(multiwalker_v3.env())

    env = env_creator({})
    ## ipark
    ##register_env("waterworld", env_creator)
    register_env("i3walkers", env_creator)

    obs_space = env.observation_space
    act_space = env.action_space

    config = {
        "model": {
            "fcnet_hiddens": [400, 300],
            "fcnet_activation": "relu",
        },
        "gamma": 0.99,
    }
    policies = {agent: (None, obs_space, act_space, config) for agent in env.agents}


    tune.run(
        ## ipark
        "PPO",
        #checkpoint_freq=10,
        stop={"episodes_total": 100000},

        config={
            # Enviroment specific                                        
            ## ipark
            "env": "i3walkers", 
                                                                         
            # General
            "log_level": "ERROR",
            "num_gpus": 0, #"num_gpus": 1,
            "num_workers": 3, #"num_workers": 8,
            "num_envs_per_worker": 3, #"num_envs_per_worker": 8,
            "compress_observations": False,
            "gamma": .99,
                                                                         
            "lambda": 0.95,
            "kl_coeff": 0.5,
            "clip_rewards": True,
            "clip_param": 0.1,
            "vf_clip_param": 10.0,
            "entropy_coeff": 0.01,
            ###"train_batch_size": 5000,
            ###"sgd_minibatch_size": 500,
            ###"lr": 5e-5,
            "train_batch_size": 50000,
            #"sgd_minibatch_size": 5000,
            "sgd_minibatch_size": 500,
            "lr": 0.0001,
            ############################

            "num_sgd_iter": 10,
            "batch_mode": 'truncate_episodes',
            "vf_share_layers": True,
                                                                         
                                                                         
            # Method specific
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": (lambda agent_id: agent_id),
            },
        },
    )
