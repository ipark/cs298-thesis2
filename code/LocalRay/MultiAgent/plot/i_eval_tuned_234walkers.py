import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_4c317_00000_0_2020-11-16_19-52-22'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='1-Walker', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i2walkers_18ca9_00000_0_2020-11-12_17-53-59'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='2-Walkers', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_180d7_00000_0_2020-11-14_15-57-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
plt.plot(x, y, label='3-Walkers', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_32e7d_00000_0_2020-11-15_12-43-21'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='4-Walkers', linewidth=2)


plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Env:Multil-Walkers | Model:Hidden[400,300]\n  Policy:PPO Independent Learning | batchSize=50K | miniBatch=5K')

plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000], #, 110000, 120000, 130000, 140000, 150000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k']) #, '110k', '120k', '130k', '140k', '150k'])

#plt.xlim(0, 150000)
plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0, 50, 100, 150],labels=['-150','-100','-50','0', '50', '100', '150'])
plt.ylim(-150, 200)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()

