import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

#df["rolling"] = df["number_users"].rolling(1).mean()

"""
data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_5ec2f_00000_0_2020-10-28_18-45-06'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_32e7d_00000_0_2020-11-15_12-43-21'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='walker2dParamSet', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_952fc_00000_0_2020-11-03_10-37-10'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='MineSet', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_44ee3_00000_0_2020-10-26_14-14-44'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='MineSet-[miniBatchSize=500]', linewidth=2, alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Env:4-Walkers | Model:Hidden[400,300]\n  Policy:PPO Independent Learning | batchSize=50K | miniBatch=5K')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000], #, 110000, 120000, 130000, 140000, 150000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k']) #, '110k', '120k', '130k', '140k', '150k'])

plt.xlim(0, 150000)
#plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0, 50, 100, 150],labels=['-150','-100','-50','0', '50', '100', '150'])
plt.ylim(-150, 150)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()

