
set(self):
        return self.env.reset()


current_phase = 0

def manage_curriculum(info):
    global current_phase
    print("Manage Curriculum callback called on phase {}".format(current_phase))
    result = info["result"]
    if result["episode_reward_mean"] > 30:
        current_phase+=1
        agent = info["agent"]
        #agent.optimizer.foreach_evaluator(lambda ev: ev.env.set_phase(current_phase))
        agent.optimizer.foreach_evaluator(lambda ev: [e.set_phase(current_phase) for e in ev.async_env.get_unwrapped()])



if __name__ == "__main__":
    ray.init()
    tune.run_experiments({
        "test": {
            "run": "PPO",
            "env": TestEnv,
            "config": {
                "num_workers": 3,
                "num_envs_per_worker": 10,
                "callbacks": {
                    "on_train_result": tune.function(manage_curriculum),
                },
            },
        },
    })
