import sys
from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.pettingzoo_env import PettingZooEnv
from pettingzoo.sisl import multiwalker_v3

# Based on code from github.com/parametersharingmadrl/parametersharingmadrl

if __name__ == "__main__":
    def env_creator(args):
        ## ipark
        return PettingZooEnv(multiwalker_v3.env())

    env = env_creator({})
    ## ipark
    ##register_env("waterworld", env_creator)
    register_env("i2walkers", env_creator)

    obs_space = env.observation_space
    act_space = env.action_space

    config = {
        ###"model": {
        ###    "fcnet_hiddens": [400, 300],
        ###    ###"fcnet_activation": "relu",
        ###},
        "gamma": 0.99,
    }
    policies = {agent: (None, obs_space, act_space, config) for agent in env.agents}


    tune.run(
        ## ipark
        "PPO",
        stop={"episodes_total": 100000},
        checkpoint_freq=10,
         
        #config:                               
        #    # Works for both torch and tf.
        #    framework: tf
        #    ##########################
        #    kl_coeff: 1.0
        #    num_sgd_iter: 20
        #    lr: .0001
        #    ###sgd_minibatch_size: 32768
        #    ###train_batch_size: 320000
        #    ###num_workers: 64
        #    ###num_gpus: 4
        #    sgd_minibatch_size: 5000
        #    train_batch_size:  50000
        #    num_workers: 3
        #    num_gpus: 0
        #    batch_mode: complete_episodes
        #    observation_filter: MeanStdFilter
        #    ##########################
        config={
            # Enviroment specific                                        
            ## ipark
            "env": "i2walkers", 
                                                                         
            # General
            "framework": 'tf',
            "kl_coeff": 1.0,
            "num_sgd_iter": 20,
            "lr": .0001,
            "sgd_minibatch_size": 5000,
            "train_batch_size":  50000,
            "num_workers": 3,
            "num_gpus": 0,
            "batch_mode": 'complete_episodes',
            "observation_filter": 'MeanStdFilter',
                                                                         
            # Method specific
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": (lambda agent_id: agent_id),
            },
        },
    )
