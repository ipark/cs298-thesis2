import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

#df["rolling"] = df["number_users"].rolling(1).mean()

data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_5ec2f_00000_0_2020-10-28_18-45-06'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_f7057_00000_0_2020-11-01_15-50-04'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i3Walkers [batchSize=50K; miniBatchSize=5K]', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_72eb6_00000_0_2020-10-26_04-36-12'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i3Walkers [batchSize=5K; miniBatchSize=500]', linewidth=2, color='red', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('3-Walkers | Hidden[400,300] w/ Relu \n Policy: PPO; Independent Learning')
plt.xticks(ticks=[10000,30000,50000, 70000,90000,110000, 130000, 150000, 170000],
labels=['10k','30k','50k','70k','90k','110k','130k','150k','170k'])

plt.xlim(0, 180000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-150, 50)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()
