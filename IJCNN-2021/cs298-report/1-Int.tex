%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction}
\vspace{20pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRO-1
\section{Motivation}
\noindent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The end goal of this project is how to find the optimal sequential actions 
(walking and carrying a package) for the multi-objectives (walkers with robot-legs) to take
leading them to move forward as far as possible to maximize the accumulated score (reward)
using Deep Reinforcement Learning.

%=======================
\subsection{Why Deep Reinforcement Learning (DRL)?
It's Closer to Artificial General Intelligence.}\noindent
%=======================
This project was motivated by seeking an AI method more towards
Artificial General Intelligence (AGI), i.e. more similar to learning behavior of 
human-beings. As of today, Deep Reinforcement Learning (DRL) is the most
closer to the AGI compared to other machine learning methods. 
As shown in \Cref{f:ml} Supervised Learning (SL) is
one-shot learning, i.e. its prediction is either true or false
with respect to the target label of training data;  
Without target labels, Unsupervised Learning (UL) is able to extract complex patterns from 
the given data. Both SL and UL are passive and static leaning by minimizing the 
prediction errors from the offline static data. 
In contrast, DRL (DL + RL = DRL)
is active and dynamic learning method owing to an \textbf{agent} that interacts with 
its \textbf{environment} (i.e. takes actions to environment and incorporates the 
feedback from environment) through generating online experience data while learning. 

%
\begin{figure}[th!]
	\centering
	\includegraphics[width=0.8\columnwidth]{./Figs/intro/SL-UL-RL.png}
    \caption{Reinforcement Learning is neither SL nor UL. Deep Learning + Reinforcement = DRL}
	\label{f:ml}
\end{figure} 

Human-beings are rather active and dynamic learners by {\bf{interacting with
environment}}, not just passively learning the given information at that moment, 
but also {\bf{exploring}} more information by taking the previous experiences (ups and downs
envisaged as rewards or penalties) into consideration relentlessly. 
Also, to achive a specific goal, human-beings learn {\bf{sequentially}} (not just one time)
by {\bf{evaluating own actions}} followed by accumulating experience in their own
learning {\bf{strategy (aka policy)}} to {\bf{maximize the return}} 
(not just immediate reward, but also long-term 
accumulated rewards).  Such description provides a high-level analogy to the technical 
explanation of the DRL in \Cref{ch:DRL}.


%=======================
\subsection{Why Walkers (Robot-Legs) Environment? 
It's Very Intuitive DRL Application to Real-World Robotics.}\noindent
%=======================
AlphaGo has gotten public attention as a prototype of advanced AI technology 
to realization exceeded human intelligence. Core driving force of AlphaGo
is ``Deep'' RL, which uses neural network (like DL) in conjunction 
to RL.  
Since then DRL has been a popular AI method applied to various fields, namely, 
robotics for manipulating locomotion,  healthcare for
protein folding prediction \cite{protein}, operational research for adaptive scheduling \cite{jssp},
resource management for heterogeneous 5G network \cite{5G}, etc. 

Here for the thesis project, we select a simple walker system (a part of robot system, 
consisting of several joints of legs and thighs) 
as an environment for the DRL based on the following two reasons:

Firstly, robot locomotion is straightforward to understand what we want to achieve without prior domain knowledge
(robotics environments are collected for the DRL researchers \cite{pybullet,roboschool}),
hence I can focus on the algorithmic aspect of DRL without distraction of application specific knowledge.

Secondly, learning/training the walkers environment requires high-dimensional and continuous action space 
(not discrete action space) thus challenging to train. Yet, walkers environment has 
reduced degrees of freedom from legs/thighs instead of whole body, thus it's feasible to train
given the available time and computing resource allowed for this thesis project. 
\Cref{ch:exp} provides a detailed specification of the walkers environment.

%=======================
\subsection{Why Multi-Agent? 
Challenging Problems Can be Solved Cooperatively by Sharing Information.}\noindent
%=======================
\Cref{t:DRL-DP-GT} shows that 
DRL is closely related to Dynamic Programming in the single-agent settings;
also related to the Game Theory in the multi-agent settings.

%\item \textbf{Single-Agent DRL vs. Dynamic Programming (DP): } 
In single-agent settings, for both DRL and Dynamic Programming (DP) as optimization methods, 
DP enumerates all the possible states hence limited to small scale problems,
whereas DRL uses sampling and neural network as a function approximator thus
can be can scalable to larger problems.
% 
\begin{table}[htb!]
\caption{Comparison: DRL vs. Dynamic Programming vs. Game Theory}
\label{t:DRL-DP-GT}
\centering
\begin{tabular}{
M{0.16\textwidth}|
M{0.3\textwidth}|
M{0.3\textwidth}}
\hline\hline
\vspace{10pt}
Suitable Scale & 
\vspace{10pt}
Single-Agent & 
\vspace{10pt}
Multi-Agent \\[15pt]
\hline
Large-Scale Problems & \textbf{DRL}  & \textbf{Multi-Agent DRL} \\[15pt]
\hline
Small-Scale Problems & Dynamic Programming  & Game Theory        \\[15pt]
\hline\hline
\end{tabular}
\end{table}
%
\begin{table}[htb!]
\caption{Comparison: DRL vs. Game Theory}
\label{t:DRL-GT}
\centering
\begin{tabular}{
M{0.4\textwidth}|
M{0.4\textwidth}
}
\hline\hline
\vspace{10pt}
DRL         & 
\vspace{10pt}
Game Theory\\[15pt]
\hline
\vspace{5pt}
Agent       & 
\vspace{5pt}
Player\\[5pt]
Environment & Game\\[5pt]
Policy      & Strategy\\[5pt]
Reward      & Payoff\\[5pt]
State       & Information State\\[5pt]
Greedy Policy & Best Response\\[5pt]
\hline\hline
\end{tabular}
\end{table}
%

%\item \textbf{Multi-Agent DRL (MADRL) vs. Game Theory : } 
Aa listed in \Cref{t:DRL-GT} Multi-Agent DRL (MADRL) and Game Theory share similar concept
almost one-on-one conceptual matching between them just represented by different terminologies.
That is, both MADRL and Game Theory behave similarly with common goal to 
maximize accumulated reward and total payoff, respectively.

%\item \textbf{Cooperation in Multi-Agent DRL (MADRL) : }
In multi-agent settings, multiple agents share a single environment with each other agent,
thus the optimal policy of single-agent depends not only on the environment, 
but on the policies of the other agents. That's why multi-agent DRL is challenging
than single-agent settings suffering two problems: 
1) non-stationarity and 2) high variance of estimated values.
Theses challenges in MADRL can be mitigated by \textbf{cooperative} learning strategies
such as \textbf{parameter sharing} and other training strategies 
in a way to improve the stationarity as well as to reduce the variance.  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRO-2
\section{Aims \& Research Questions}\noindent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
At the end of this work, we'd like to address the following questions
based on the observations from the DRL runs with
\textbf{Agent} (Proximal Policy Optimization) and
\textbf{Environment} (single and multi-(2-, 3-, and 4-) walkers).

Three \textbf{broad conceptual questions} are:
1) How to capture the true reward signal of environment to achieve a goal?
2) Mutual transfer learning is feasible in DRL between single- and multi-agent settings?
and 3) In multi-agent settings, parameter sharing is better than independent learning?

In addition, two \textbf{specific technical questions} will be answered:
1) How to find the tuned hyperparameters in DRL to maximize the performance?
2) How to balance training bias vs. variance in DRL?

The answers to those questions listed above would contribute to 
not only enhancing the conceptual understanding of DRL problems, 
but also providing technical information with DRL training strategies (little different from DL)
together with an algorithm-specific  tuned hyperparameter set. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRO-3
\section{Summary of Accomplishments \& Overall Structure}\noindent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Cref{f:acc} highlights major achievements from this study.  
Overall structure of this report is following:
\Cref{ch:DRL} will provide background information on conceptual aspect of DRL, 
component of DRL, classification of DRL methods, and key equations of DRL.
Learning strategies and architectures of Multi-Agent DRL will be described in \Cref{ch:MADRL}.
Experimental settings for DRL framework and specifications of walkers environments will be given in \Cref{ch:exp}.
Mainly six observations will be described and corresponding supporting information will be discussed in 
\Cref{ch:RnD}.  We will conclude with lessons learned from this project and suggest future direction in 
\Cref{ch:end}.

\begin{figure}[th!]
\centering
\fbox{
\includegraphics[width=\columnwidth]{./Figs/revision/accomplishments.png}
}
\caption{Accomplishments of This Study}
\label{f:acc}
\end{figure} 

