from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.pettingzoo_env import PettingZooEnv
## ipark
##from pettingzoo.sisl import multiwalker_v2
# local env
from sisl import multiwalker_ipark

# Based on code from github.com/parametersharingmadrl/parametersharingmadrl

if __name__ == "__main__":
    # RDQN - Rainbow DQN
    # ADQN - Apex DQN
    def env_creator(args):
        ## ipark
        ##return PettingZooEnv(waterworld_v0.env())
        ##return PettingZooEnv(waterworld_v1.env())
        return PettingZooEnv(multiwalker_ipark.env())

    env = env_creator({})
    ##ipark
    ##register_env("waterworld", env_creator)
    register_env("twoWalkers", env_creator)

    obs_space = env.observation_space
    act_spc = env.action_space

    policies = {agent: (None, obs_space, act_spc, {}) for agent in env.agents}

    tune.run(
        ## ipark
        ##"APEX_DDPG",
        "PPO",
        stop={"episodes_total": 60000},
        checkpoint_freq=10,
        config={
            # Enviroment specific
            ##ipark
            ##"env": "waterworld",
            "env": "twoWalkers",

            # General
            ## ipark
            #"num_gpus": 1,
            "num_gpus": 0,
            "num_workers": 3,
            # Method specific
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": (lambda agent_id: agent_id),
            },
        },
    )
