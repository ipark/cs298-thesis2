from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.pettingzoo_env import PettingZooEnv
## ipark
##from pettingzoo.sisl import waterworld_v0
##from pettingzoo.sisl import waterworld_v1
from pettingzoo.sisl import multiwalker_v2

# Based on code from github.com/parametersharingmadrl/parametersharingmadrl

if __name__ == "__main__":
    # RDQN - Rainbow DQN
    # ADQN - Apex DQN
    def env_creator(args):
        ## ipark
        ##return PettingZooEnv(waterworld_v0.env())
        ##return PettingZooEnv(waterworld_v1.env())
        return PettingZooEnv(multiwalker_v2.env())

    env = env_creator({})
    ##ipark
    ##register_env("waterworld", env_creator)
    ##register_env("three_walkers", env_creator)
    register_env("four_walkers", env_creator)

    obs_space = env.observation_space
    act_spc = env.action_space

    policies = {agent: (None, obs_space, act_spc, {}) for agent in env.agents}

    tune.run(
        ## ipark
        ##"APEX_DDPG",
        "PPO",
        stop={"episodes_total": 60000},
        checkpoint_freq=10,
        config={
            # Enviroment specific
            ##ipark
            ##"env": "waterworld",
            ##"env": "three_walkers",
            "env": "four_walkers",

            # General
            ## ipark
            #"num_gpus": 1,
            "num_gpus": 0,
            "num_workers": 3,
            # Method specific
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": (lambda agent_id: agent_id),
            },
        },
    )
