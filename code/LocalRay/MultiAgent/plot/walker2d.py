import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os



data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_32fe2_00000_0_2020-11-08_02-43-12'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=5K; Tuned(Walker2d)]', linewidth=2)

data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_ba2d1_00000_0_2020-11-07_19-23-10'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=500; Tuned(Walker2d)]', linewidth=2)


data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_c2b65_00000_0_2020-11-08_16-01-47'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=50; Tuned(Walker2d)]', linewidth=2)

####
data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_dc7f2_00000_0_2020-11-07_03-17-45'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=5K; MineSet(Multi-Walker)]', linewidth=2)



#data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_65c10_00000_0_2020-11-05_17-50-07'
data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_81fc3_00000_0_2020-11-06_12-34-45'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=500; MineSet(Multi-Walker)]', linewidth=2)





data_path = '/Users/ipark/ray_results/walker2d-v2-ppo/PPO_Walker2d-v2_e9908_00000_0_2020-11-05_21-28-33'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='[minibatchSize=50; MineSet(Multi-Walker)]', linewidth=2)


plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Walker2d TunedParamSet vs. MineSet(Multi-Walker)\n  TrainBatchSize=50K; Various MiniBatchSize')
plt.xticks(ticks=[0, 20000,40000,60000,80000, 100000],labels=['0', '20k','40k','60k','80k', '100k'])
plt.xlim(0, 100000)
plt.yticks(ticks=[0, 1000, 2000, 3000, 4000, 5000],labels=['0', '1000', '2000', '3000', '4000', '5000'])
plt.ylim(0, 5000)
plt.legend(loc='best')
plt.margins(x=0)

plt.show()


