from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.pettingzoo_env import PettingZooEnv
##from pettingzoo.sisl import waterworld_v0
## ipark
##from pettingzoo.sisl import multiwalker_v2
# local env
from sisl import multiwalker_ipark
import ray.rllib.rollout as rollout

# Based on code from github.com/parametersharingmadrl/parametersharingmadrl

if __name__ == "__main__":
    # RDQN - Rainbow DQN
    # ADQN - Apex DQN
    def env_creator(args):
        ## ipark
        ##return PettingZooEnv(waterworld_v0.env())
        return PettingZooEnv(multiwalker_ipark.env())

    env = env_creator({})
    ## ipark
    ##register_env("waterworld", env_creator)
    register_env("fourWalkersParamShare", env_creator)


    # resume run (not working)
    ### python multi_agent_parameter_sharing_4walkers.py \
    ### ~/ray_results/PPO/PPO_fourWalkersParamShare_5680d_00000_0_2020-09-26_08-24-15/\
    ### checkpoint_660/checkpoint-660 \
    ### --run PPO \
    ### --env fourWalkersParamShare \
    ### --no-render \
    ### --track-progress \
    ### --episodes 60000 \
    ### --out rollouts.pkl
    ###parser = rollout.create_parser()
    ###args = parser.parse_args()
    ###rollout.run(args, parser)

    obs_space = env.observation_space
    act_space = env.action_space

    policies = {"shared_policy": (None, obs_space, act_space, {})}

    # for all methods
    policy_ids = list(policies.keys())


    tune.run(
        ## ipark
        "PPO",
        stop={"episodes_total": 60000},
        checkpoint_freq=10,
        config={

            # Enviroment specific
            ## ipark
            ##"env": "waterworld",
            "env": "fourWalkersParamShare",


                # General
                "log_level": "ERROR",
                "num_gpus": 0, #"num_gpus": 1,
                "num_workers": 3, #"num_workers": 8,
                "num_envs_per_worker": 3, #"num_envs_per_worker": 8,
                "compress_observations": False,
                "gamma": .99,


                "lambda": 0.95,
                "kl_coeff": 0.5,
                "clip_rewards": True,
                "clip_param": 0.1,
                "vf_clip_param": 10.0,
                "entropy_coeff": 0.01,
                "train_batch_size": 5000,
                "sgd_minibatch_size": 500,
                "num_sgd_iter": 10,
                "batch_mode": 'truncate_episodes',
                "vf_share_layers": True,

            # MLP hidden layer 
            "model":{
                "fcnet_hiddens" : [400, 300],
            },

            # Method specific
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": (lambda agent_id: "shared_policy"),
            },
        },
    )
