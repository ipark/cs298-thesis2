import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

#df["rolling"] = df["number_users"].rolling(1).mean()

data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'sa_ppo.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-i3Walkers [400,300] Ref.***', linewidth=3, color='cyan', alpha=0.6)

data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_193c4_00000_0_2020-10-24_16-39-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-1Walker', linewidth=2, color='orange', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_i2walkers_91717_00000_0_2020-10-25_20-30-17'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i2Walkers', linewidth=2, color='red', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_72eb6_00000_0_2020-10-26_04-36-12'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i3Walkers', linewidth=2, color='blue', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_44ee3_00000_0_2020-10-26_14-14-44'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='PPO-i4Walkers', linewidth=2, color='green', alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


"""
data_path = '/Users/ipark/ray_results/bipedalwalker-v3-ppo/PPO_BipedalWalker-v3_46e61_00000_0_2020-10-02_18-10-50'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-bipedalwalker [400,300]', linewidth=1)

data_path = '/Users/ipark/ray_results/PPO/PPO_i2walkers_dff8f_00000_0_2020-10-01_09-19-26'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-i2Walkers [400,300]', linewidth=1)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_3b7ca_00000_0_2020-09-30_21-11-51'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
#df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-i3Walkers [400,300]', linewidth=1)
#
data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_4cb5e_00000_0_2020-10-01_20-06-43'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-i3Walkers(2) [400,300]', linewidth=1)

data_path = '/Users/ipark/ray_results/PPO/PPO_i4walkers_30e23_00000_0_2020-10-01_15-19-37'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-i4Walkers [400,300]', linewidth=1)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_68e5f_00000_0_2020-10-02_11-09-27'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-1Walker [400,300]', linewidth=1)
"""

plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Independent Learning : PPO Hidden[400,300] Relu')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k'])

plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-200, 50)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()
