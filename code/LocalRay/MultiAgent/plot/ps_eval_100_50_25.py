import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'PPO1.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-3Walkers [400,300] Ref.***', linewidth=3, color='cyan')

data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_7f534_00000_0_2020-10-04_13-30-52'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-2Walkers [100,50,25]', linewidth=1, color='gray')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

"""
data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_ebaac_00000_0_2020-10-07_11-14-20'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-2Walkers [600,300,150]', linewidth=1)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_2aedb_00000_0_2020-10-04_20-59-29'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-3Walkers [100,50,25]', linewidth=1, color='blue')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_4walkers_d7b16_00000_0_2020-10-03_17-52-13'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
df = df[['episodes_total', "episode_reward_mean", "episode_reward_min", "episode_reward_max"]]
data = df.to_numpy()
plt.plot(data[:, 0], data[:,1], label='PPO-4Walkers [100,50,25]', linewidth=1, color="black")
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('ParamSharing')
plt.xticks(ticks=[10000,20000,30000,40000,50000,60000,70000,80000,90000,100000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k'])
#plt.xlim(0, 60000)
plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-200, 50)
plt.legend(loc='lower center')
plt.margins(x=0)

plt.show()

