#!/bin/bash
rllib train --run PPO --env Walker2d-v2 -f walker2d-ppo.tune500.yaml
sleep 10;
rllib train --run PPO --env Walker2d-v2 -f walker2d-ppo.tune5000.yaml
sleep 10;
rllib train --run PPO --env Walker2d-v2 -f walker2d-ppo.tune50.yaml
sleep 10;
