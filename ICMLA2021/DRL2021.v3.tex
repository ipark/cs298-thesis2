\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}

% ipark
\usepackage{multirow}
\usepackage{hyperref} 
\hypersetup{hidelinks} % Do not include boxes around links
\usepackage{cleveref} 
\usepackage{enumitem} 
\setlist[itemize]{noitemsep, nolistsep}
\setlist[itemize,1]{label={-}}
\setlist[itemize,2]{label={$\cdot$}}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Multi-Agent Deep Reinforcement Learning for Walker Systems}

\author{
\IEEEauthorblockN{Inhee Park}
\IEEEauthorblockA{\textit{Department of Computer Science} \\
\textit{San Jose State University}\\
San Jose, CA, USA\\
ipark.c@gmail.com}
\and
\IEEEauthorblockN{Teng-Sheng Moh}
\IEEEauthorblockA{\textit{Department of Computer Science} \\
\textit{San Jose State University}\\
San Jose, CA, USA\\
teng.moh@sjsu.edu}
}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
\noindent
We applied the state-of-art performance Deep Reinforcement Learning 
(DRL) algorithm, Proximal Policy Optimization (PPO), to the minimal 
robot-legs locomotion for the challenging multi-agent, continuous and 
high-dimensional state-space environments. The main contribution of 
this work is identifying the potential factors/hyperparameters and 
their effects on the performance of the multi-agent settings by varying 
the number of agents. Based on the comprehensive experiments with 2-10 
multi-walkers environments, we found that 1) A minibatch size and a 
sampling reuse ratio (experience replay buffer size containing multiple 
minibatches) are critical hyperparameters to improve performance of 
the PPO; 2) Optimal neural network size depends on the number of walkers 
in the multi-agent environments; and 3) Parameter sharing among 
multi-agent is a better training strategy than fully independent 
learning in terms of comparable performance and improved efficiency 
with reduced parameters consuming less memory.
\end{abstract}

\begin{IEEEkeywords}
Deep Reinforcement Learning  (DRL), Proximal Policy Optimization (PPO),
Multi-agent DRL (MADRL)
\end{IEEEkeywords}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Reinforcement Learning (RL) consists of an agent (a policy as an 
intelligent part) and an environment (specification of states, actions 
and reward functions). To optimize a policy, we need to evaluate it by: 
1) Value-function based method; 2) Policy-gradient based method with 
a parameterized policy incorporated into a neural network; and 
3) Actor-Critic method in combination of the value-function and the 
policy-gradient to reduce a variance of the estimated values and 
policy-gradient \cite{1}.

We choose a simple walker environment consisting of several joints 
of legs, thighs and a hull, because 1) robot locomotion is 
straightforward to set the end-goal, i.e. moving walkers as far as 
from a starting point, hence we can focus on the algorithmic aspect 
of Deep Reinforcement Learning (DRL) without domain-specific knowledge; 
and 2) the walker's environment has reduced degrees of freedom from 
legs/thighs instead of a whole body, thus it's feasible to train 
without enormous amounts of computing resources.

By definition, a multi-agent system is a loosely coupled network of 
agents working together to solve problems that are beyond individual 
capability \cite{3}. A single walker environment (e.g. BipedalWalker 
\cite{10}) is considered as a solved problem, but a simple variant 
by adding a package on top of a single walker to carry becomes a 
challenging problem by dropping performance. To solve this challenging 
environment (e.g. newly created environment Multi-walkers \cite{13}, 
we consider multi-agent DRL (MADRL) in which multiple walkers (one 
agent per walker or a shared agent among walkers) walk and carry a 
package together. In general, due to the shared environment among 
multiple agents, MADRL is known to suffer 1) non-stationarity and 
2) high variance of the estimated values and policy-gradient. 
These challenges can be mitigated by centralized learning and 
decentralized execution (CLDE) and a cooperative learning strategy 
such as parameter sharing \cite{6,18}.

Experience Replay Buffer is an efficient and stable data-generating 
mechanism in DRL by storing collected transitions for training,
but it is limited to off-policy DRL methods. Asynchronous Advantage 
Actor-Critic (A2C) method uses parallelized workers with different 
random seeds to generate uncorrelated data, which can be used for 
on-policy DRL methods. Zhang and Sutton noted that the DRL community 
seems to have a default value for the size of the replay buffer
(i.e. 1-million) but it is a task-dependent hyperparameter to be
considered as a sensitive hyperparameter \cite{5}.  Similarly, 
minibatch sizes and experience reuse ratios are also considered 
affecting the quality of gradient update, as sensitive hyperparameters 
\cite{19,20}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Works}
\subsection{Proximal Policy Optimization (PPO)}
The PPO is an on-policy policy-gradient method with four notable 
features for reducing a variance in estimated values and policy-gradient 
\cite{2}: 1) Similar to its precursor on-policy method, A2C, concurrent 
workers/actors generate a broad set of experience samples in parallel 
to mitigate a data correlation as well as to utilize a multi-core system; 
2) Experience replay buffer is implemented in the actor-critic architecture.  
The actors/workers interact with the environment to gather experience 
samples and store them to the experience replay buffer. Once fulfilling 
the training buffer size, a uniformly drawn minibatch size sub-sample 
from the buffer is used to train the Critic network as a policy evaluation. 
Then the actor network updates the parameterized policy as a policy 
improvement; 3) Clipping the policy and value function restricts a drastic 
change of the gradient, thus clipping strategy not only prevents a 
divergence but also allows a reuse of the experience replay buffer by 
drawing multiple mini-batches; and 4) Clipped policy objective function 
uses Generalized Advantage Estimator (GAE) \cite{17}. GAE uses an 
exponentially weighted combination of n-step advantage function targets 
achieving a substantial reduction in the variance.

\subsection{Centralized Learning with Decentralized Execution}
In MADRL, an environment is shared by multiple agents, i.e. what one 
agent learns becomes outdated as other agents also learn simultaneously. 
This makes observations non-stationary, resulting in difficulty of 
convergence as well as a high variance in the estimated values. 
The Actor-Critic architecture (Figure 5) for the single-agent setting 
can be extended to the MADRL with the one of multi-agent training 
frameworks (complete survey is available in \cite{18}): Centralized
Learning with Decentralized Execution (CLDE) \cite{6}. The CLDE is 
designed to reduce the non-stationarity by centralized learning 
during training, but each multi-agent executes an action based on 
its own local observation.

\subsection{Previous study on multi-walker environment}
There are two related MADRL studies using the same multi-walker 
environments. First, Gupta et al. \cite{7} created the multi-walker 
environment (3-walker as a default) based on the single BipedalWalker 
environment \cite{10}. They implemented a single-agent algorithm, 
Trust Region Policy Optimization (TRPO), which is a precursor algorithm 
of the PPO, in conjunction with the parameter sharing strategy for the 
MADRL \cite{7}. With fully independent learning (they used the term 
`concurrent learning') as a performance baseline, they applied parameter 
sharing versions of DRL algorithms to multi-walker environments. 
However, their resulting performance of the MADRL was not scalable as 
the number of agents increased, thus so called ``Curriculum Learning'' 
(CL) was used on top of the parameter sharing version of TRPO at the 
cost of extra training time for CL. Note that in our study, we rather 
focus on hyperparameter tuning of the PPO, yielding a much higher 
performance than their combined TRPO and CL.  Second, Terry et al. 
\cite{8} compared the MADRL strategies between parameter sharing with 
respect to the baseline strategy of independent learning using a total 
12 different DRL algorithms, including the PPO, by applying to three 
different multi-agent environments. One of them is a 3-walker environment 
(multi-walker with three agents). Based on their extensive testing, 
they provided a tuned hyperparameter set for PPO as well as other DRL 
algorithms \cite{13}. Note that in Table III, we compared our tuned 
hyperparameter set along with theirs.  

\subsection{Independent Learning vs. Parameter Sharing}
In MADRL, there are two extreme cases in terms of sharing information 
among agents: fully independent learning and parameter sharing. 
One one hand, independent learning is the most decentralized but naive 
MADRL. Multiple single agents are trained independently based on their 
own local observation in which the existence of other agents are 
implicitly recognized as a part of the environment. As to increase 
the number of agents, no scalability is expected because each agent 
should generate its own experience samples without sharing with other 
actors/workers, thus memory capacity becomes an issue.  

On the other hand, parameter sharing is the most centralized MADRL 
method without explicit communication among agents. Multiple agents 
share the network parameters of actor-network and critic-network. 
This is an appropriate learning strategy for the homogeneous agents. 
For those similar or identical agents, if one agent has less chance to 
learn from the environment, such agent is beneficial by following the 
shared policy that was properly updated by other agents. Parameter 
sharing strategy also shares the rewards, which encourages the multiple 
agents to participate cooperatively to accomplish a common goal. 
Unlike independent learning, parameter sharing is scalable because 
experiences gathered from multiple actors/workers are all shared in 
the experience replay buffer.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments}
\subsection{Environments}
A single walker environment is classified as a solved problem once the 
average maximum reward is achieved a certain threshold (e.g. 300 \cite{12}).
 There are benchmark studies with a single walker environment to compare 
which DRL algorithm achieves the maximum score in fewer trials \cite{16}. 
The PPO algorithm outperforms especially for the single walker-like 
environments. Thus, we use the PPO as a main DRL algorithm. For our
hyperparameter tuning experiments, we utilize parameter transferability 
between single-agents (both BipedalWalker \cite{12} and Walker2d \cite{16}) 
and  multi-agent (multi-walkers [9]) settings depicted in Figure 1. 
Table I summarizes walker-like environments that we used for hyperparameter 
tuning. All the DRL runs carried out are summarized in Table II.
%%%%%%%%%%%%%%%%%%%%% f:allWalkers (fig1)
\begin{figure}[th!]
\centering
	\includegraphics[width=\columnwidth]{./Figs/envall.png}
    \caption{All walkers environment considered in this work}
	\label{f:allWalkers}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%% t:related (table1)
\begin{table}[htb!]
\caption{Summary of related works}
\label{t:related}
\centering
\begin{tabular}{|c|c|c|}
\hline
%  
Agent  & Environment   & Algorithm\\
\hline
Single & Walker2d      & PPO \cite{14}\\
Multi  & 3-Walkers     & PPO \cite{8} \\
Multi  & Multi-Walkers & TRPO + Curriculum Learning \cite{7}\\
\hline
Multi & 1-10 Walkers  & PPO (this work)\\
\hline
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%% t:related (table2)
\begin{table}[htb!]
\caption{Comprehensive list of DRL experiments}
\label{t:allExp}
\centering
\begin{tabular}{|l|}
\hline
\textbf{Agent}\\
\begin{minipage}[t]{0.4\textwidth} 
\begin{itemize}
    \item Single-PPO
    \item Multi-PPO (Parameter Sharing, Independent Learning)
\end{itemize}
\end{minipage}
\\\hline
\textbf{Environments}  \\
\begin{minipage}[t]{0.4\textwidth} 
\begin{itemize}
    \item Single-Walker 
        \begin{itemize}
            \item Bipedal Walker                    
            \item A Package Carrying Bipedal Walker
            \item MuJoCo Walker2d
        \end{itemize}
    \item Multi-Walkers 
        \begin{itemize}
            \item A Package Carrying [1-10]-Walkers 
        \end{itemize}
\end{itemize}
\end{minipage}
\\\hline
\textbf{Hyperparameters} \\
\begin{minipage}[t]{0.4\textwidth} 
\begin{itemize}
    \item General DRL Training Related 
        \begin{itemize}
            \item Reward Clipping   
            \item Size of Hidden Layers in Neural Network
        \end{itemize}
    \item PPO Related
        \begin{itemize}
            \item Minibatch Size 
            \item Experience Replay Buffer Size = Training Batch Size
            \item Reuse Ratio = (Training Batch Size)/(Minibatch Size)
            \item Clipping Policy Coefficient
            \item Clipping Value-function Coefficient
            \item Kullback–Leibler (KL) divergence Initial Coefficient
        \end{itemize}
\end{itemize}
\end{minipage}
\\\hline
\end{tabular}
\end{table}

\subsection{MADRL Framework and Agents/Policies}
We use RLlib, one of DRL frameworks \cite{11}, which has implemented an 
advantage actor-critic PPO algorithm. Moreover, many state-of-art DRL 
algorithms support both single-agent and multi-agent settings in a form 
of python API. For the single-agent PPO, the RLlib has a module to 
register the OpenAI's gym environments for BipedalWalker as well as 
Walker2d \cite{10}. For the multi-agent PPO, RLlib provides an interface 
called PettingZoo, a collection of multi-agent environments \cite{13}. 
We can thus use a multi-walker environment by using the RLlib's 
multiagent module.  However, we have to modify the PettingZoo's 
default 3-walker environment to create 1-, 2- , and 4 through 10-walker 
environments. Inside the multiagent module of RLlib, we can set 
configuration parameters either for parameter sharing or for independent 
learning. For the parameter sharing, we set the same shared policy ID 
to all multi-agents. In contrast, for independent learning, we set 
distinct policy IDs to each of multi-agents. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results \& Discussion}

\subsection{Synergic Effect of Combined Optimal Hyperparameters Improves the Performance}
As a baseline performance, we used the tuned PPO hyperparameter set for 
the multi-agent 3-walkers environment \cite{8}. Given the similarity 
of walker-like environments as depicted in Figure 1, we also use another 
tuned PPO hyperparameter set for the single-agent Mujoco Walker2d 
environment from the large-scale benchmark \cite{14}. The hyperparameters 
are compared with our tuned values denoted in bold faces in Table III. 
Figure 2 shows 8 different runs of PPO using different hyperparameter 
sets from less performed sub-optimal sets (gray solid line) up to well 
performed optimal sets (blue solid line). The order of contribution to 
the performance is: 1) Turning off the clipping reward; 2) Increased 
minibatch size to 5,000; 3) Decreased node size of neural network; and 
4) Less strict clipping policy coefficient to 0.3. The best performance 
of the PPO for the 3-walkers environment is achieved by combined 
contributions of those hyperparameters. Each of these hyperparameters 
are examined in the next sections.
%%%%%%%%%%%%%%%%%%%%% f:findingContribution (fig2)
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/synergic.png}
\caption{Finding the most contributed hyperparameter to the performance for the  
3-walker environment by changing the parameter one at a time}
\label{f:findingContribution}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%% f:rewardClipping (fig3)
\begin{figure}[th!]
\includegraphics[width=\columnwidth]{./Figs/rewardShaping.png}
\caption{Effect of reward clipping on the performance of 1 through 4-walkers environments
with reward clipping (a) and without reward clipping (b)}
\label{f:rewardClipping}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%% t:ppo-rllib (table3)
\begin{table}[htb!]
\caption{Comparison of PPO hyperparameters
among default values of the RLlib implementation, 
previous works for 3-Walkers~\cite{8},
Walker2d~\cite{14} and 1-10 Walkers (this work)}
\label{t:ppo-rllib}
\centering
\begin{tabular}{|l|l|l|l|}
\hline
%
Environment                        & 3-Walkers    & Walker2d         & 1-10 Walkers\\
\hline                                                                    
\textbf{Hyperparameters}   & & & \\
(RLlib default)    & & & \\
clip\_param             (0.3)                   & 0.1         & 0.2      & default\\ 
kl\_coeff               (0.2)                   & 0.5         & 1.0      & 1.0\\
num\_sgd\_iter          (30)                   & 10          & 20       & 20 \\
entropy\_coeff          (0.0)                  & 0.01        & default  & default\\
sgd\_minibatch\_size    (128)                  & 500         & 32,768   & \textbf{5,000}\\
train\_batch\_size      (4,000)                 & 5,000       & 320,000  & \textbf{50,000}\\
use\_gae                (True)                 & default     & default  & default\\ 
lr                      (5e-5)                 & default     & 1e-4     & 1-e4\\  
\hline
model:vf\_share\_layers                         &             &          &        \\
(False)                 & True        & default  & default\\
\hline
batch\_mode                                     &             &          &          \\
(truncate\_episodes)                            & default     & complete & complete \\
\hline
clip\_rewards          &             &          &         \\
(None)                 & True        & default  & default \\
\hline
observation\_filter     &             &          &         \\
(NoFilter)             & default     & MeanStdFilter  & MeanStdFilter\\
\hline
\end{tabular}
\end{table}

\subsection{Reward Shaping is Crucial for Proper Goal Setting in DRL}
As noted in Table III, Terry et al. set the clip\_rewards=True. Reward 
clipping normalizes a reward range into (-1, 1) by taking a sign of 
values (e.g. np.sign(values) in Numpy). In general, it is used when 
comparing performances among diverse environments with various reward 
ranges. In Figure 3a, reward clipping suppresses the improvement of 
performance, its maximum mean reward is less than 10 for all 1 through
4-walkers environments. Even if a walker fell or a package fell, 
a minimum penalty (-1) is assigned instead of intended penalty (-100), 
which doesn't motivate the walker to learn how to stand and walk. 
This reward shaping alters the end-goal of the environment. When we 
observe the learning behavior of walkers from the roll-out simulation, 
walkers tend to jump and roll frequently instead of standing and moving 
forward. In contrast, in Figure 3b, turning off the reward clipping 
restores the intended reward function assigning a proper reward range 
from -100 to 1 proportional to the displacement of a package location.
Reward-function is predefined as an environment specific manner, which 
can't be controlled by an agent. Agents can only receive the emitted 
reward signal from the environment and select an optimal sequence of 
action to maximize the accumulated reward. Designing reward functions 
(also called Credit Assignment) is one of the research fields in DRL
\cite{15}. With a reward clipping, the end-goal task is rather balancing 
a package while standing at the same location, whereas without a reward 
clipping the end-goal task is walking and moving forward. Therefore, 
depending on the reward shaping, a goal task of DRL can be altered.


\subsection{Optimal Minibatch Size and Sampling Reuse Ratio are 
Important Hyperparameters of PPO to Improve the Performance}

In DRL, an experience replay buffer is a data-generation mechanism
collected by the policy, which improves sample efficiency by enabling 
multiple reuse of the samples. The parameterized policy in DRL is 
optimized ($\theta \leftarrow \theta + \alpha\nabla J(\theta)$)
by policy-gradient based method with respect to the expected return, $G(\tau)$.
\begin{equation}
\nabla J(\theta)=\underset{\tau\sim\mathcal{U(D}(\theta))}
{\mathbb{E}}\big[G(\tau)\,\nabla_{\theta}\log\;\pi(a|s)\big].
\end{equation}
In the PPO, $J(\theta)$ is a clipped policy objective function
%\begin{equation}
$J(\theta,\theta^-)=\underset{(s,a,A^{GAE})\sim\mathcal{U(D}(\theta^-))}
{\mathbb{E}}\big[\min(P1, P2)\big]A^{GAE}$, where
%\end{equation}
$P1 = \frac{\pi(a\mid s;\theta)}{\pi(a\mid s;\theta^-)}$,
$P2 = \text{clip}\big(      
\frac{\pi(a\mid s;\theta)}{\pi(a\mid s;\theta^-)}, 1-\epsilon; 1+\epsilon \big)$.
The policy generated experiences are stored in the experience replay 
buffer up to a training batch size; minibatch-sized samples are drawn 
from it on every optimization step; gradients are 
computed and averaged over multiple minibatches, and old policy ($\theta^-$)
is updated with new one with ($\theta$).

\Cref{f:minibatchVarying} shows that the best performance is achieved 
with a minibatch size of 5K and training batch size of 50K for the 
3-walkers environment (blue curve).  Corresponding sample reuse ratio 
is 10 (minibatch-size/experience-buffer-size), which implies that the 
network updates with 10 different minibatches drawn from the experience 
replay buffer. The optimal hyperparameters of minibatch and training 
batch sizes can be depicted in \Cref{f:ppo-now}.
%%%%%%%%%%%%%%%%%%%%% f:minibatchVarying (fig4)
\begin{figure}[th!]
\centering
\includegraphics[width=0.8\columnwidth]{./Figs/minibatch.png}
\caption{Performance of 3-walkers environment with various 
minibatch size and train batch size.  Bluish curves present
the performances when the sample reuse ratio of 10;
greenish curves for the sample reuse ratio of 2}
\label{f:minibatchVarying}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%% f:ppo-now  (fig5)
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/minibatchSize.png}
\caption{PPO algorithm with optimized experience replay buffer size and minibatch size}
\label{f:ppo-now}
\end{figure}

The original PPO algorithm shares neural networks for both a policy 
and a value function \cite{4}, but the PPO implementation of the RLlib 
package provides a parameter, model:vf\_share\_layers (see Table III), 
to use either separate networks or a shared network. In this study, 
we set a model:vf\_share\_layers=False to use separate networks. 

Cobbe et al. developed `Phasic Policy Gradient' (PPG) \cite{16}
as an improved version of PPO: 1) separating the shared network of PPO 
into policy-network and value-network; 2) allowing the policy-network 
and value-network are trained with different control parameters such as 
policy-epoch $E_{\pi}$ and value-epoch $E_v$; 3) improving performance
for diverse single-agent environments using $E_v$ of 9 for the 
value-network and $E_{\pi}$ of 1 for the policy-network. 

This is consistent with our observations with multi-agent walker
environment in \cref{f:loss}, where almost negligible loss of policy-gradient, 
whereas noisy loss of value-function.  The value function loss requires 
more updates to be optimized, thus the better performance is achieved 
with more number of sample reuse ratio of 10 than 2.
Qualitatively the core driving force of DRL is a reward function. 
The degree of a diversity of the gradients \cite{19} computed from the 
experience in the minibatch/experience replay buffer captured the true 
reward signal of the multi-walker environment.
%%%%%%%%%%%%%%%%%%%%% f:NNsize (fig6a->fig6)
\begin{figure}[th!]
\centering
\includegraphics[width=0.8\columnwidth]{./Figs/loss.png}
\caption{Training loss of policy-gradient vs. value-function}
\label{f:loss}
\end{figure}
%%%%%%%%%%%%%%%%%%%%% f:NNsize (fig6b->fig7)
\begin{figure}[th!]
\centering
\includegraphics[width=0.8\columnwidth]{./Figs/NNsize.png}
\caption{Varying a node size of two hidden layers in DRL}
\label{f:NNsize}
\end{figure}

\subsection{Larger Capacity of Neural Network is Better for More Number 
of Multi-Agent Environment}
Henderson et al. investigated sources of variance in reported results 
of DRL, extrinsic factors such as hyperparameters and intrinsic factors 
such as environment properties (single-agents) \cite{14}. One of the 
investigated hyperparameters was hidden multilayer perceptron (MLP, 
fully connected layers) architectures: fc[64, 64], fc[100, 50, 25], 
and fc[400, 300], which are commonly seen in the literature. 
The 2-hidden layers with each node side of fc[400, 300] is also used 
in the two previous works for the multi-walker environments \cite{7,8}.

Possible factors affecting to the optimal MLP architecture in DRL are: 
1) Size of input/output layer. For example, each walker of the 
multi-walker environments consists of 32-dimensional state-space 
and 4-dimensional action-space. In the PPO architecture, the policy 
network is state-in and action-out configuration, that is, the size 
of input and output layer is 32 and 4, respectively; 
2) Amount of training data available. For example, amount of training data 
is optimally found with minibatch size of 5K and train-batch size of 50K. 

However, there is no clear rule in determining the optimal MLP architecture 
suitable for multi-walker environments.  Thus we vary the size of each 
hidden with respect to the commonly used fc[400, 300] architecture. 
\cref{f:NNsize} summarizes the resulting maximum mean reward for 3-walkers 
and 8-walkers environments. Maximum performance is achieved with a 
smaller network fc[100,50] for the 3-walkers, whereas a larger network 
fc[400, 300] is best for the 8-walkers. In this MADRL study, we assumed 
that multi-agents cooperate via implicit communication by observing the 
existence of other agents as a part of the shared environment. 
Thus, as the number of walkers increased from 3 to 8, we needed a 
large enough network to represent a more diverse state due to an 
increased number of neighboring walkers.

\subsection{Parameter Sharing is More Efficient Strategy than Fully 
Independent Learning in Multi-Agent DRL}
Now we applied our tuned hyperparameter set (see Table III) to the 
1-walker all the way to 10-walkers environments using both MADRL 
strategies of a parameter sharing (PS) and a fully independent 
learning (IL).  We can observe three groups of environments in terms of
their perfomance shown in Figure 8:
1) a scalable growth group (1 through 4-5 walkers),
2) a plateau group (around 5-6-walkers),
3) a degradation group  (7-10 walkers). 

In Figure 8a and b, comparing the MADRL performance between PS and IL 
with fc[100,50], there is no significant difference between these 
training strategies in the scalable growth group and the degradation group.
However, in the plateau group (5-6 walkers), IL performed better than PS. 
As the number of agents increases, the observational space of the shared 
environment among multi-agents becomes saturated with the fc[100,50] network, 
thus IL performs better than PS. 

Figure 8c and d demonstrate the MADRL performance results with fc[400,300] 
for PS and IL, respectively. In the scalable group, IL is better to 
achieve higher mean reward.  However, unlike fc[100,50], performance is 
degraded over 200K episodes for 3-4 walkers.  In the degradation group, 
PS outperformed IL.  These different observations imply that smaller 
network fc[100,50] is better tuned hyperparameter for the 1-5 walkers 
environments, whereas the larger network fc[400,300] is optimal 
hyperparameter for the 7-10 walkers.

To sum up, we found the optimal hyperparameter set for the multi-agent 
PPO algorithm, which improves the performance much better than other 
studies for the common environment of 3-walkers in Table IV. Finally, 
we tabulate the maximum mean rewards achieved by the PPO-based MADRL 
runs for multi-walkers shown in Table V. For 2-7 walkers, the higher 
maximum mean rewards are obtained by the IL strategy,whereas for 8-10 
walkers, the PS performed better than IL. Those values as a performance 
metric should be considered together with the overall performance graphs 
in Figure 8.
%%%%%%%%%%%%%%%%%%%%% t:performances-our (table4)
\begin{table}[htb!]
\caption{Comparison of Performance for the 3-Walkers Environment}
\label{t:performance-our}
\centering
\begin{tabular}{|c|c|c|c|}
\hline
\multirow{2}*{Reference}   & \multirow{2}*{Algorithm}  & Max Mean Reward      & Max Mean Reward \\
                           & & Indep. Learning      & Param Sharing\\\hline
\cite{7} & TRPO       & 51 & 54\\\hline 
\cite{8} & PPO        & 38 & 41\\\hline
This work & PPO    & \textbf{169} & \textbf{168}\\\hline
\end{tabular}
\end{table}

If we consider the possible variance in the mean rewards from multiple 
runs for each environment, it seems that PS and IL perform similarly. 
The PS method requires less number of parameters than IL because of 
the shared policy. In addition, the PS method takes much less memory 
than the IL method because it has a shared experience replay buffer 
collected from the multiple agents involved in that environment. Given 
the comparable performance between PS and IL, considering together with 
the number of parameters and required resources, the PS is a more 
efficient strategy of MADRL than the IL.
%%%%%%%%%%%%%%%%%%%%% f:PSvsIL (fig7)
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/ab.png}
\includegraphics[width=\columnwidth]{./Figs/cd.png}
\caption{Performance comparison between parameter sharing (a) vs. independent learning (b)
for 1-10 walkers environments with fc[100,50] network;  
parameter sharing (c) vs. independent learning (d) with fc[400,300] network}
\label{f:PSvsIL}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%% t:performances-all (table5)
\begin{table}[htb!]
\caption{Maximum average reward in 300K (1-5 walkers) or 500K (6-8 walkers) episodes}
\label{t:performance-all}
\centering
\begin{tabular}{|c|cc|cc|}
\hline
Number of   & Independent & Hidden  & Parameter & Hidden\\
Walkers     & Learning    & NN Size & Sharing   & NN Size\\
\hline
1  &  \multicolumn{4}{c}{27 @272K,  fc[100,50]}\\
\hline
2 &  86 @288K & [100,50]  &  \textbf{95} @133K & [100,50]  \\
3 & \textbf{169} @259K & [100,50]  &   168 @287K & [100,50]  \\
4 & \textbf{222} @209K & [100,50]  &   215 @265K & [100,50]  \\
5 & \textbf{271} @266K & [100,50]  &   245 @257K & [100,50]  \\
\hline
6 & \textbf{261} @291K & [100,50]  &   249 @326K & [200,100]  \\
7 & \textbf{265} @298K & [100,50]  &   255 @497K & [200,100]  \\
8 & 261 @400K & [200,100] &   \textbf{300} @498K & [400,300] \\
%  &            &           &   267 @375K & [300,150] \\
%  &            &           &   260 @439K & [200,100] \\
%  &            &           &   213 @298K & [100,50] \\
9  & 224 @498K & [100,50]  & \textbf{273} @500K  & [400,300] \\
10 & 196 @500K & [400,300] & \textbf{225} @499K  & [400,300] \\
\hline
\end{tabular}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion \& Future Work}
Based on the findings from our MADRL experiments for the multi-agent 
walkers using PPO algorithm, we can generalize the following: First, 
unlike DL with explicit target labels, DRL needs a larger minibatch 
size to better estimate policy gradients. Therefore, the minibatch 
size and its superset experience replay buffer size are critical
hyperparameters in the PPO algorithm. Second, with a determined minibatch 
size, a smaller size of neural network provides a stable and higher 
performance for 1-5 walker environments, whereas larger capacity neural 
networks are necessary for 6-10 walker environments. Finally, for the 
homogeneous multi-agent environments trained with well tuned 
hyperparameters, the parameter sharing is a better strategy for the 
MADRL in terms of performance and efficiency with reduced parameters 
and less memory.

The main contribution of this work is identifying the potential 
factors/hyperparameters, in particular, a minibatch size and a neural 
network size, and their effects on the performance of the PPO in the 
multi-agent settings by varying the number of agents.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{8}
\bibitem{1}
Benhamou, E. and Saltiel, D.: 
Similarities between policy gradient methods in reinforcement 
and supervised learning, ESANN 2020 proceedings, European Symposium 
on Artificial Neural Networks, Computational Intelligence and Machine Learning. 
pp. 721, Online event, 2-4 October (2020)
				
\bibitem{2}
Morales, M.: Grokking Deep Reinforcement Learning, Shelter Island, NY, USA: Manning Publications 
(2020)
				
\bibitem{3}
Jennings, N. R., Sycara, K. and Wooldridge, M.: A Roadmap of Agent Research and 
Development, Autonomous Agents and Multi-Agent Systems, vol. 1, pp. 7--38 (1998)
	
\bibitem{4}
Schulman, J., Wolski, F., Dhariwal, P., et al.:
Proximal Policy Optimization Algorithms, CoRR abs/1707.06347 (2005)

\bibitem{5}
Zhang, S.and Sutton, R. S.: A Deeper Look at Experience Replay, 
CoRR abs/1712.01275, 2020.

\bibitem{6}
Lowe, R. Wu, Y., Tamar, A., et al.:
Multi-Agent Actor-Critic for Mixed Cooperative-Competitive Environments in 
Proceedings of the 31st International Conference on Neural Information Processing 
Systems (NIPS'17), Curran Associates Inc., Red Hook, NY, USA, pp. 6382-–6393 (2005)
					
\bibitem{7}
Gupta, J., Egorov, J., and Kochenderfer, M.: 
Cooperative Multi-agent Control Using Deep Reinforcement Learning in Lecture Notes 
in Computer Science, vol. 10642, Autonomous Agents and Multiagent Systems. 
G.  Sukthankar and J. Rodriguez-Aguilar Ed. 
Springer, Cham. (2005)
					
\bibitem{8}
Terry, J., Grammel, N., Hari, A., et al.:
Revisiting Parameter 
Sharing In Multi-Agent Deep Reinforcement Learning, CoRR abs/2005.13625 (2020)
					
\bibitem{9}
Liang, E. and  Liaw, R.: Scaling Multi-Agent Reinforcement Learning, 
\url{https://bair.berkeley.edu/blog/ 2018/12/12/rllib}. 
Last accessed Nov 2020
					
\bibitem{10}
BipedalWalker-v2, 
\url{https://gym.openai.com/envs/BipedalWalker-v2}.
Last accessed Nov 2020
					
\bibitem{11}
Goldberg, K., Gonzalez, J., Jordan, M. and Stoica, I.: 
RLlib: Abstractions for Distributed Reinforcement Learning in Proceedings of the 
35th International Conference on Machine Learning, vol. 80, pp. 30530-3062 (2018)
					
\bibitem{12}
Ray v1.1.0dev0 Documentation: RLlib: Scalable Reinforcement Learning, 
\url{https://docs.ray.io/en/master/rllib.html}
Last accessed Nov 2020
					
\bibitem{13}
PettingZoo: Multi-Agent Reinforcement Learning Environments, 
\url{https://www.pettingzoo.ml}
Last accessed Nov 2020
					
\bibitem{14}
Henderson, P., Islam, R., Bachman, P., et al.:
Deep Reinforcement Learning that Matters, CoRR abs/1709.06560 (2019)
					
\bibitem{15}
Ng, A., Harada, D. and Russell, S.: 
Policy invariance under reward transformations: theory and application to reward 
shaping in Proceedings of the Sixteenth International Conference on Machine Learning, 
Bled, Slovenia (1999)
					
\bibitem{16}
Cobbe, K., Hilton, J., Klimovand, O., et al.:
Phasic Policy Gradient, CoRR abs/2009.04416 (2020)
					
\bibitem{17}
Schulman, J., Moritz, P.,  Levine, S., et al.:
High-Dimensional
Continuous Control Using Generalized Advantage Estimation, in 4th International 
Conference on Learning Representations (ICLR 2016). San Juan, Puerto Rico, May 2-4, (2016)

\bibitem{18}
Gronauer, S. and Diepold, K.: 
Multi-agent deep reinforcement learning: a survey, 
Artificial Intelligence Review, pp. 1573-7462 (2021)

\bibitem{19}
Yin, D., Pananjady, A., Lam, M.,  et al.:
Gradient Diversity: a Key Ingredient for Scalable Distributed Learning, 
CoRR abs/1706.05699 (2017)

\bibitem{20}
Fedus, W., Ramachandran, P., Agarwal, R. et al.:
Revisiting Fundamentals of Experience Replay, CoRR abs/2007.06700 (2020)
\end{thebibliography}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}

