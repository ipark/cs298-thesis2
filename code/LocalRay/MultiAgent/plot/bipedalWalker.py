import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

data_path = '/Users/ipark/ray_results/bipedalwalker-v3-ppo/PPO_BipedalWalker-v3_37381_00000_0_2020-11-11_13-59-48'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='BipedalWalker [batchSize=50K, minibatchSize=5K]', linewidth=2)


data_path = '/Users/ipark/ray_results/bipedalwalker-v3-ppo/PPO_BipedalWalker-v3_46e61_00000_0_2020-10-02_18-10-50'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"]
data = df.to_numpy()
plt.plot(x, y, label='BipedalWalker [batchSize=5K, minibatchSize=500]', linewidth=2)


plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('[Single Agent] Env: BipedalWalker-v3 | Policy: PPO')
plt.xticks(ticks=[0,10000,20000,30000,40000,50000],labels=['0', '10k', '20k','30k', '40k','50k'])
plt.xlim(0, 50000)
plt.yticks(ticks=[-100, -50, 0, 50, 100, 150, 200, 250, 300, 350],labels=['-100', '-50', '0', '50', '100', '150', '200', '250', '300', '350'])
plt.ylim(-120, 350)
plt.legend(loc='best')
plt.margins(x=0)

plt.show()

