%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Results \& Discussion\label{ch:RnD}}
\vspace{20pt}
\noindent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-1
\section{Reward Shaping is Crucial for Proper Goal Setting in DRL}\label{result-1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}[th!]
\fbox{\includegraphics[width=\columnwidth]{./Figs/revision/finding1.png}}
\caption{Effect of reward clipping on the performance of 2-, 3-, and 4-walkers environments
with reward clipping (a) vs without reward clipping (b). 
Note that for (b) we changed other hyperparameters
but turning off the reward clipping was the dominant parameter to scale up the values (y-axis).
In (a) the ``PPO 3-walkers with $[400,300]$ Ref***'' refers to the result from \cite{Terry},
they used reward clipping.}
\label{f:rewardClipping}
\end{figure} 
%
\subsection*{Observation.}
%=======================
\noindent
Initially we used the tuned parameters set (see \Cref{f:hyperparams}a)
for the 3-walkers environment from the work by Terry et al. \cite{Terry} 
One of parameter was \texttt{clip\_rewards=True}. In general, reward clipping
is used only when comparing performances among many environments with various
reward range.  Reward clipping was designed to normalize a reward range $\left[-1, 1\right]$ by
taking a sign of the value-function (e.g. \texttt{np.sign(values)} in Numpy).

As observed in \Cref{f:rewardClipping}a), reward clipping suppresses
the improvement of performance, its maximum mean reward is less than 10
for all 1-, 2-, 3-, and 4-walkers environments.

Even if a walker fell or a package was fallen, a minimum reward of -1 was assigned to the
corresponding action, which doesn't motivate standing or walking. If we observed
from the roll-out simulation, indeed walkers tend to jump and roll frequently instead of standing and moving forward.

As observed in \Cref{f:rewardClipping}b), turning off the reward clipping
restore the intended reward function, the mean rewards keep increased up to the maximum mean rewards 
of $\sim$ 50, 100, and 150 for 2-, 3-, and 4-walkers, respectively.

Intended reward function was assigning a proper reward ranging from -100 to 1 proportional to the
displacement of a package location.  If walker fell or a package was fallen, the corresponding action
should be assigned -100, which demotivates falling rather motivates walking and moving forward.

Although we should not use reward clipping for performance comparison
of walkers environments, two interesting observations using reward clipping 
were 1) difficulty of convergence in loss function during training indicates
that the reward signal from environment is not corrected reflected;
2) suppressed mean reward range revealed subtle effect of hyperparameters
on the performance, otherwise its effect was diluted in the large mean
reward values.


\subsection*{Interpretation \& Supporting Information.}
%====================================================
\noindent
Reward-function is pre-defined as an environment specific manner, which can't be controlled by an agent.
Agent can only received the emitted reward signal from environment and select an optimal sequence of
action to maximize the accumulated reward. 

Designing reward function (also called Credit Assignment) 
is one of research fields in DRL \cite{CreditAssign}.
As observed in our cases with reward clipping the goal task is rather balancing a package while standing
at the same location, wheres without reward clipping the goal task is restored to walking and moving forward. 
Therefore, depending on the reward shaping, a goal task of DRL can be altered.
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-2
\section{Larger Capacity of Neural Network is Better for High-Dimensional, Continuous Observation Space}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}[th!]
\centering
\fbox{\includegraphics[width=\columnwidth]{./Figs/revision/finding2.png}}
\caption{Effect of NN capacity on the performance of PPO.
Note that the ``PPO 3-walkers with $[400,300]$ Ref***'' refers to the result from \cite{Terry}.}
\label{f:NNsize}
\end{figure} 
%
\subsection*{Observation.}
%=======================
\noindent
The mean reward from PPO with larger capacity of NN with larger number of nodes (e.g. $\left[400, 300 \right]$)
shows positive (\Cref{f:NNsize}a),
whereas that with smaller capacity of NN with more number of layers (e.g. $\left[100, 50, 25\right]$)
shows smaller values moreover very noisy.  

\subsection*{Interpretation \& Supporting Information.}
%====================================================
\noindent
Due to high dimensional continuous 
state/action-space in the 2-, 3-, 4-walkers environments,
the larger size of NN has better capability of differentiating 
from state to state. 

Henderson et al. benchmark general NN training parameters (such 
as NN capacity, NN architecture, activation function, optimizer etc.) and
specific hyperparameters of many DRL algorithms (Deep Q-learning, 
A2C, PPO, etc.) for the frequently used DRL 
single-agent environments. \cite{DRLmatters}

NN capacity is one of hyperparameters and optimal capacity depends
on the environment whether discrete state/action-space or continuous, high-dimensional
actions-space. Unless our environment has small scale state/action-space,
it would be safe to use relatively larger capacity such as 2-hidden layer with
size of $\left[400, 300\right]$.

DRL suffers non-stationary due to lack of supervised
target label. To mitigate that non-stationary problem,
two strategies are suggested by using 
1) a target network for temporary stationary
and 2) a large enough network for telling a difference  
between similar states due to correlation among generated experience data.
\cite{GDRL2020}
%
%

\clearpage
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-3
\section{Optimal Minibatch Size and Sampling Reuse Ratio are Important Hyperparameters
of PPO to Improve the Performance\label{result-3}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}[th!]
\centering
    (a)\\
    \includegraphics[width=0.9\columnwidth]{./Figs/result/pettingzoo-ppo.png}\\
    (b)\\
    \includegraphics[width=0.5\columnwidth]{./Figs/result/rllib-ppo.png}\\
    \caption{PPO hyperparameter set from other works: 
Ref.~\cite{Terry} for 3-Walker multi-agent environment (a)
and from Ref.~\cite{RLlib-paper} for MuJoCo Walker2d single-agent environment (b)}
	\label{f:hyperparams}
\end{figure}
%
\subsection*{Observation.}
%======================
\noindent
\Cref{f:hyperparams}a) and b) show the tuned hyperparameter set of PPO for 
3-walkers and single-walker, respectively, from other researchers. 
There are several differences to be aware of:
1) The former is multi-agent environment, whereas the latter is single-agent;
2) The former uses multi-core CPUs, whereas the latter used multi-GPUs; 
3) Besides a problematic hyperparameter of \texttt{clip\_rewards=True} as 
already discussed in \Cref{result-1}, we notice rather drastic differences in their 
minibatch size as well as train batch size;
4) But the ratio between train batch size over minibatch size is approximately similar to 10.

Given their computing system difference (CPU vs GPU), they may find a different tuned use different sizes.
Direct use of their tuned hyperparameters to our environment is not transferable.
Therefore, we should tune minibatch and train batch size optimal to our computing system (4-core CPUs).

\Cref{f:minibatchVarying} shows that the tuned hyperparameter set (5K minibatch; 50K train batch)
outperform for all 2-, 3-, and 4-walkers environments.
%
\begin{figure}[th!]
\fbox{\includegraphics[width=\columnwidth]{./Figs/revision/finding3a.png}}
\caption{Larger minibatch size of 5K improves performance of PPO by 
providing assorted gradients for better value estimate}
\label{f:minibatchVarying}
\end{figure}
%
While fixing the experience replay buffer size at 50K, 
we change minibatch size to experiment with different sampling reuse ratio.
\Cref{f:reuseVarying} shows that more sampling reuse of 10 improve performance of
PPO because value-function needs more number of optimization.
%
\begin{figure}[th!]
\centering
\fbox{\includegraphics[width=\columnwidth]{./Figs/revision/finding3b.png}}
\caption{Effect of varying reuse ratio on the PPO performance 
for 3-walkers environment (left); 
corresponding training loss of policy-gradient vs. value-function (right)}
\label{f:reuseVarying}
\end{figure}


\subsection*{Interpretation \& Supporting Information.}
%====================================================
\noindent
In DL, there is some consensus that smaller batch size is better for generalization than larger batch size,
perhaps owing to the supervised learning, thus free from non-stationary issue unlike DRL.
DRL is usually suffers non-stationary, known as ``Moving Target'' problem.
Hence, a large enough train batch is needed to compute more various gradient descent, thus more frequent
update from minibatch size stochastic gradient descent (SGD) for better optimization.
\cite{ReplayBuffer1,ReplayBuffer2}

Cobbe et al. \cite{PPG} improved version of PPO, called `Phasic Policy Gradient' (PPG):
1) separating the shared network of PPO into policy-network and value-network;
2) allowing the policy-network and value-network are trained with different reuse ratio from minibatch/train-batch;
3) improved performance than PPO from diverse single-agent environment using larger reuse ratio  (9)
for the value-network and smaller reuse ratio (1) for the policy-network.

This is consistent with our multi-agent walkers environment:
1) we found that reuse ratio of 10 outperformed than 2; given that PPO is shared network,
thus we can only use same reuse ratio for both policy-network and value-network.
this implies that policy-part of network converges faster within 2 updates,
the value-part of network converges slowly, thus need more update like 10.  
2) as shown in the inset graph in \Cref{f:reuseVarying} 
we confirmed our conjecture from the almost negligible loss of policy-gradient, whereas
yet noisy loss of value-function from our training log.
%
\begin{figure}[th!]
\centering
\fbox{\includegraphics[width=\columnwidth]{./Figs/revision/minibatchSize.png}}
\caption{PPO Algorithm with Optimized Experience Replay Buffer Size and Minibatch Size}
\label{f:ppo-now}
\end{figure}
%
In short, as depicted in Actor-Critic PPO algorithm in \Cref{f:ppo-now},
DRL may need larger sizes of train batch and minibatch that SL approach of DL.
Thus, minibatch size together with its reuse ratio is an important hyperparameter specific for the 
environment as well as computing system.
Also, as already discussed in \Cref{section-ppo},
using a shared single network for both policy and value functions (feature of A2C algorithm,
which is one of components of the PPO) is beneficial for computational efficiency, 
however, at a potential risk of different scales of the policy and value function updates. 

\clearpage
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-4
\section{Synergic Effect of Combined Optimal Hyperparameters\label{result-4}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection*{Observation.}
%======================
\noindent
Together with the tuned  optimal minibatch size and train batch size discussed in \Cref{result-3},
we tried to use other tuned PPO hyperparameters
from the single-agent environment (Walker2d) from benchmark work. \cite{DRLmatters}

\Cref{f:findingContribution} shows 7 different runs of PPO using different hyperparameter set
from less performed sub-optimal set (gray solid line) up to well performed optimal set (black solid line).
We found that the contribution to the performance is significant in this order:
1) 
Turning off the reward clipping - this is rather general training control of DRL than policy algorithm
(see detailed discussion in \Cref{result-1});
2) 
Minibatch size of 5K (see detailed discussion in \Cref{result-3});
3) 
Clipping policy coefficient of 0.3 than 0.1 (less strict constraint, thus allows more diverse gradient);
and 
4) KL divergence initial coefficient.
However, the best performance of PPO for the multi-agent environments is achieved
by combined contribution of those hyperparameters.
%
%
\begin{figure}[th!]
\centering
(a)\\
    \includegraphics[width=0.7\columnwidth]{./Figs/result/ComparisonHyperparam.png}\\
(b)\\
    \fbox{\includegraphics[width=\columnwidth]{./Figs/revision/finding4.png}}
    \caption{Comparison of PPO hyperparameter set between single-agent MuJoCo Walker2d vs.
our sub-optimal hyperparameter set for multi-agent walkers (a);
Finding the most contributed hyperparameter to the performance for the  3-walker environment 
by changing the parameter one at a time (b)}
	\label{f:findingContribution}
\end{figure}
%

\subsection*{Interpretation \& Supporting Information.}
%====================================================
\noindent
By increasing the clipping policy coefficient $\epsilon$ to 0.3 from 0.1 (denoted as red color),
we can select new policy little bit more deviated from the old policy providing more diverse gradients.
This can be observed by red and green curve in \label{f:findingContribution}b)
where the mean reward is increased earlier with $\epsilon$ to 0.3 than 0.1 (i.e curve is shifted left).
\begin{align}
J(\phi,\phi^-)=\underset{(s,a,A^{GAE})}{\mathbb{E}}\Bigg[\min\Bigg( 
& \frac{\pi(a\mid s;\phi^-)}{\pi(a\mid s;\phi)}\; A^{GAE},\\\nonumber
& {\color{red}            
\text{clamp}\bigg(      
\frac{\pi(a\mid s;\phi^-)}{\pi(a\mid s;\phi)}\; A^{GAE}; 1-\epsilon; 1+\epsilon
\bigg)
}
& \Bigg)
\Bigg]
\end{align}
%

The increased $\epsilon$ together with the larger minibatch size of 5K
provides well distributed samples  the experience replay buffer, which 
in turn resulting in decent performance improvement.
Moreover, $A^{GAE}$, the advantage function estimated by Generalized Advantage Estimator (GAE)
from the experience buffer, reduces variance (i.e. noise in DRL) resulting in better performance of PPO.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-5
\section{Mutual Transfer of Tuned Hyperparameters between Single-Agent (SA)  and Multi-Agent (MA) Environments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Transfer hyperparameters from MA to SA environment}
%=============================================
\noindent
In \Cref{result-3}, we found the optimal minibatch/train-bath size of 5K/50K from 2-, 3-, and 4-walkers environment.
We try to transfer this hyperparameters to similar but single-agent walker environment.
As demonstrated in \Cref{f:transferSA2M}b) and c), hyperparameters from MA is fully transferable to SA
for both Walker2d (little bit different legs configuration, where there are feet but no hull)
and BipedalWalker (identical legs configuration to multi-walkers but without carrying a package on top).
\begin{figure}[th!]
\centering
    (a)\\
    \includegraphics[width=0.9\columnwidth]{./Figs/result/transfer-multi2single.png}\\
    (b)\hspace{200pt}(c)\\
    \includegraphics[width=0.49\columnwidth]{./Figs/result/walker2dWorks.png}                      
    \includegraphics[width=0.49\columnwidth]{./Figs/result/bipedalWorks.png}
    \caption{Hyperparameter transfer flow from MA to SA (a); 
    Resulting improvement of performance in single-agent environment: Walker2d (b) and BipedalWalker (c)}
	\label{f:transferSA2M}
\end{figure}
%

\subsection{Transfer hyperparameters from SA to MA environment}
%=============================================
\noindent
As was already discussed already in \Cref{result-4}
hyperparameters are compared between single-agent Walker2d and multi-walkers in \Cref{f:findingContribution}a).
Tuned hyperparameter set of Walker2d with minibatch/train-bath size of 5K/50K
can be fully transferred to all 2-, 3-, and 4-walkers environments,
i.e. dramatically improved performance of PPO (\Cref{f:transferMA2SA}b-d).
\begin{figure}[th!]
    (a)\\
    \includegraphics[width=0.8\columnwidth]{./Figs/result/transfer-walker2d-multiwalkers.png}\\
    (b)\hspace{200pt}(c)\\
    \includegraphics[width=0.5\columnwidth]{./Figs/result/2w-tuned.png}
    \includegraphics[width=0.5\columnwidth]{./Figs/result/3w-tuned.png}
    (d)\\
    \includegraphics[width=0.5\columnwidth]{./Figs/result/4w-tuned.png}
    \caption{Hyperparameter transfer flow from SA to MA (a);
    Resulting improvement of performance for 2, 3, and 4-walkers environment in (b) through (d)}
    \label{f:transferMA2SA}
\end{figure} 
%

To conclude, there is a clear mutual transferability between MA and SA
as long as their environment are similar.  It seem that 
little difference in the configuration of the environments (such as with or without feet,
or with a package or not) are tolerable and still transferable the tuned hyperparameters.


\clearpage
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULT-6
\section{Parameter Sharing vs. Independent Learning in Multi-Agent DRL}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection*{Observation.}
%======================
\noindent
As observed in \Cref{f:PSvsIL}, performance-wise (mean reward on y-axis)
there is not much difference between parameter sharing and independent learning,
albeit maximum average reward is little bit higher in independent learning 
(\Cref{t:performance-all}). 

In parameter sharing multi-agents, all PPO runs for 
2-, 3- and 4-walkers were all completed up to 10K episode.   
However in independent learning multi-agent, we could not complete the PPO run
for the 4-walkers environment due to out of memory capacity issue.
Thus, it is clear  that parameter sharing is scalable and efficient strategy 
for the multi-agent DRL without any sacrifice of performance.

\begin{figure}[th!]
\centering
	\fbox{\includegraphics[width=0.99\columnwidth]{./Figs/revision/finding6.png}}
    \caption{Comparison of Multi-Agent Learning Strategies}
	\label{f:PSvsIL}
\end{figure} 
%
\subsection*{Interpretation \& Supporting Information.}
%====================================================
\noindent
Before finding the optimal tuned hyperparameters, 
the PPO runs for the multi-walker environment showed 
quite different performance between parameter sharing and independent leaning.
However, such difference was vanished after applying to the optimal 
hyperparameters to the PPO runs.
Probably such performance difference indicates difficulty of convergence of training
(thus required tuning the hyperparameters)
rather than behavioral difference of parameter sharing vs. independent learning.
%

To sum up, we found the optimal hyperparameter set for the multi-agent PPO algorithm,
which improve performance much better than other studies as tabulated in \Cref{t:performance-our}.
%==============================
\begin{table}[htb!]
\caption{Comparison of Performance for the 3-Walkers Environment with Other Studies}
\label{t:performance-our}
\centering
\begin{tabular}{M{0.2\textwidth}|M{0.1\textwidth}|M{0.3\textwidth}|M{0.3\textwidth}}
\hline\hline
\vspace{5pt} Ref          
\vspace{5pt}
& 
\vspace{5pt} Agent  
\vspace{5pt}
& 
\vspace{5pt} Max Avg. Reward (Independent Learning) 
\vspace{5pt}
& 
\vspace{5pt} Max Avg. Reward (Parameter Sharing)
\vspace{5pt}
\\[15pt]
\hline
\vspace{5pt} 
Gupta et al. \cite{Gupta} 
& TRPO   & 51 & 54\\[15pt]
\hline
\vspace{5pt} Terry et al. \cite{Terry} 
& PPO    & 38 & 41\\[15pt]
\hline
\vspace{5pt} This work
& PPO    & 145 & 121\\[15pt]
\hline\hline
\end{tabular}
\end{table}
%==============================

Finally, we tabulate maximum average reward achieved from our PPO-based MADRL runs for multi-walkers
shown in \Cref{t:performance-all}.  There is a clear scalability of performance for the parameter
sharing strategy.  Yet, we still need to further investigation on the  scalability of performance 
in independent learning if more memory resource is available.
%==============================
\begin{table}[htb!]
\caption{Maximum Average Reward for Multi-Walkers Achieved in This Study in 100K Episodes (except independent
4-walkers, terminated at 38K)}
\label{t:performance-all}
\centering
\begin{tabular}{M{0.3\textwidth}|M{0.3\textwidth}|M{0.3\textwidth}}
\hline\hline
\vspace{5pt} Environment
\vspace{5pt}
& 
\vspace{5pt} Max Avg. Reward (Independent Learning) 
\vspace{5pt}
& 
\vspace{5pt} Max Avg. Reward (Parameter Sharing)
\vspace{5pt}
\\[15pt]
\hline
1-Walker    & 11 & --\\
2-Walkers   & 87 & 76\\
3-Walkers   & 145 & 121\\
4-Walkers   & {\color{red}94} & 168\\
\hline\hline
\end{tabular}
\end{table}
%==============================
