import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os


data_path = '/Users/ipark/iparkDL/CS298-Thesis2/code/ParameterSharingMADRL/plot/data/multi_walker'
df = pd.read_csv(os.path.join(data_path, 'PPO1.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers [400,300] Ref.***', linewidth=3, color='cyan', alpha=0.6)


data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_193c4_00000_0_2020-10-24_16-39-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker', linewidth=2, alpha=0.6, color='orange')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_8f6c3_00000_0_2020-10-24_22-54-35'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers [minibatchSize:500]', linewidth=2, alpha=0.6, color='red')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

#PPO_2walkers_798b1_00000_0_2020-10-28_09-13-11
data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_15204_00000_0_2020-10-27_16-21-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers [minibatchSize:5,000]', linewidth=2, color='magenta')
print(x)
print(y)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_4b948_00000_0_2020-10-25_06-59-27'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers', linewidth=2, alpha=0.6, color='blue')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_4walkers_41ea0_00000_0_2020-10-25_15-56-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-4Walkers', linewidth=2, alpha=0.6, color='green')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


#### 
data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_5ec2f_00000_0_2020-10-28_18-45-06'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker [minibatchSize:5K]', linewidth=2, alpha=0.3, color='orange')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)


data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_bf6b9_00000_0_2020-10-28_23-48-27'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-2Walkers [minibatchSize:5K]', linewidth=2, alpha=0.3,color='magenta')
print(x)
print(y)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_fc3cd_00000_0_2020-10-29_08-32-42'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers', linewidth=2, alpha=0.3, color='blue')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

"""
data_path = '/Users/ipark/ray_results/PPO/PPO_4walkers_41ea0_00000_0_2020-10-25_15-56-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-4Walkers [minibatchSize:5K]', linewidth=2, alpha=0.6, color='green')
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""



plt.xlabel('Episode', labelpad=1)
plt.ylabel('Average Total Reward', labelpad=1)
plt.title('Parameter Sharing : PPO Hidden[400,300] Relu')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k'])

plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0],labels=['-150','-100','-50','0'])
plt.ylim(-200, 50)
plt.legend(loc='lower right')
plt.margins(x=0)

plt.show()

