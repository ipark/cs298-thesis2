import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import os

"""
data_path = '/Users/ipark/ray_results/PPO/PPO_1walker_5ec2f_00000_0_2020-10-28_18-45-06'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-1Walker', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_3463a_00000_0_2020-11-09_21-18-45'
#data_path = '/Users/ipark/ray_results/PPO/PPO_2walkers_44dd4_00000_0_2020-10-14_16-02-01'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y1 = df['info/learner/shared_policy/total_loss']
y2 = df['info/learner/shared_policy/policy_loss']
y3 = df['info/learner/shared_policy/vf_loss']
#plt.plot(x, y1, label='walker2dParamSet|totalLoss', linewidth=2)
plt.plot(x, y2, label='Policy Loss', linewidth=2)
plt.plot(x, y3, label='Value Function Loss', linewidth=2)

"""
data_path = '/Users/ipark/ray_results/PPO/PPO_i3walkers_180d7_00000_0_2020-11-14_15-57-03'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df["episode_reward_mean"].rolling(1).mean()
plt.plot(x, y, label='walker2dParamSet[independentLearning]', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_9b7dc_00000_0_2020-11-12_09-36-33'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-(Clip0.3;KL1.0;VFshare(X);ClipReward(X))', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_ced2c_00000_0_2020-11-12_08-19-15'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-(Clip0.3;KL1.0;VFshare(X))', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_6b6fe_00000_0_2020-11-11_18-47-35'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-(Clip0.3;KL1.0)', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_5c981_00000_0_2020-11-11_09-00-12'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-Clip0.3', linewidth=2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_51737_00000_0_2020-11-11_11-44-31'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-KL1.0', linewidth=2)


data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_59e07_00000_0_2020-11-12_11-29-15'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet-ClipReward(X)', linewidth=2, color='black')


data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_40fd5_00000_0_2020-10-31_13-18-35'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='MineSet\n[Clip0.1,KL0.5,VFshare(O),ClipReward(O)]', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""


"""
data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_f02ee_00000_0_2020-11-03_06-43-29'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers [batchS=50K; miniBatchSize=500]', linewidth=2)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)

data_path = '/Users/ipark/ray_results/PPO/PPO_3walkers_4b948_00000_0_2020-10-25_06-59-27'
df = pd.read_csv(os.path.join(data_path, 'progress.csv'))
x = df['episodes_total']
y = df['episode_reward_mean']
plt.plot(x, y, label='PPO-3Walkers [batchSize=5K; miniBatchSize=500]', linewidth=2, alpha=0.6)
#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
"""


#plt.fill_between(data[:, 0], data[:,2], data[:,3], alpha=0.2)
plt.xlabel('Trainig Time', labelpad=1)
plt.ylabel('Loss', labelpad=1)
plt.title('Env:3-Walkers | Model:Hidden[400,300]\n  Policy:PPO Parameter Sharing | batchSize=50K | miniBatch=5K\nLoss Function')
plt.xticks(ticks=[10000,20000,30000,40000,50000, 60000, 70000,80000,90000,100000], #, 110000, 120000, 130000, 140000, 150000],
labels=['10k','20k','30k','40k','50k','60k','70k','80k','90k','100k']) #, '110k', '120k', '130k', '140k', '150k'])

plt.xlim(0, 100000)
plt.yticks(ticks=[-150,-100,-50,0, 50, 100, 150],labels=['-150','-100','-50','0', '50', '100', '150'])
plt.ylim(-5, 300)
plt.legend(loc='upper right')
plt.margins(x=0)
plt.tight_layout()
plt.show()

